.globl _start
_start:
    mov sp,#0x00008000 ;@ sp <- 0x00008000
    bl blinky          ;@ call blinky
hang:
    b hang             ;@ goto hang

.globl get32
get32:
    ldr r0,[r0]        ;@ r0 <- *r0
    bx lr              ;@ return

.globl put32
put32:
    str r1,[r0]        ;@ *r0 <- r1
    bx lr              ;@ return

.globl stall
stall:
    bx lr

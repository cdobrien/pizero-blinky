#define FN_SELECT_REG 0x20200010
#define SET_REG       0x20200020
#define CLEAR_REG     0x2020002C
#define LED_PIN       0x00008000

#define WAIT_COUNT    400000L

extern unsigned int get32(unsigned int address);
extern void put32(unsigned int address, unsigned int value);
extern void stall(unsigned int x);

static void setUpLed(void);
static void wait(void);

int blinky()
{
    setUpLed();

    while (1)
    {
        put32(SET_REG, LED_PIN);
        wait();

        put32(CLEAR_REG, LED_PIN);
        wait();
    }

    return 0;
}

static void setUpLed()
{
    unsigned int r;

    r = get32(FN_SELECT_REG);

    r &= ~(7 << 21);
    r |= 1 << 21; 

    put32(FN_SELECT_REG, r);
}

static void wait()
{
    long int i;

    for (i = 0; i < WAIT_COUNT; i++)
        stall(i);
}

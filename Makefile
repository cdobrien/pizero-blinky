armToolchainDir=../toolchain/arm-none-eabi/bin
objectFiles=blinky.o startup.o

assembler=$(armToolchainDir)/arm-none-eabi-as
compiler=$(armToolchainDir)/arm-none-eabi-gcc
linker=$(armToolchainDir)/arm-none-eabi-ld
objcopy=$(armToolchainDir)/arm-none-eabi-objcopy

.c.o:
	$(compiler) -O2 -c $< -o $@

.s.o:
	$(assembler) $< -o $@

blinky.elf: $(objectFiles) memmap
	$(linker) -T memmap $(objectFiles) -o $@

kernel.img: blinky.elf
	$(objcopy) $< -O binary $@

build: kernel.img

clean:
	rm -f blinky.elf kernel.img $(objectFiles)

.PHONY: build clean
